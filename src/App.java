import java.util.Date;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        // khởi tạo hai đối tượng khách hàng 
        Customer customer1 = new Customer("thuong");
        Customer customer2 = new Customer("duong");

        Visit visit1 = new Visit(customer1, new Date());
        Visit visit2 = new Visit(customer2, new Date());

        // in ra màn hình 
        System.out.println(customer1);
        System.out.println(customer2);

        System.out.println(visit1);
        System.out.println(visit2);
    }
}
