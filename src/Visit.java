import java.util.Date;

public class Visit {
    private Customer customer;
    private Date date;
    private double serviceExpense;
    private double productExpense;
    public Visit(Customer customer, Date date) {
        this.customer = customer;
        this.date = date;
    }
    //  get

    public String getName() {
        return customer.getName();
    }
    
    public double getServiceExpense() {
        return serviceExpense;
    }
    public double getProductExpense() {
        return productExpense;
    }
    public double getTotalExpense(){
        return serviceExpense + productExpense;
    }

    public void setServiceExpense(double ex) {
        this.serviceExpense = ex;
    }
    public void setProductExpense(double ex) {
        this.productExpense = ex;
    }

    @Override
    public String toString() {
        return "Visit [customer=" + customer + ", date=" + date + ", serviceExpense=" + serviceExpense
                + ", productExpense=" + productExpense + "]";
    }

    

    // set 
    

    

    // phương thức khởi tạo
    
    
    
}
